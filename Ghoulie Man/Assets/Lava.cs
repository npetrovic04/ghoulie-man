﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    float scrollSpeed = 0.2f;
    Renderer lava;

    void Start()
    {
        lava = GetComponent<Renderer>();
    }

    void Update()
    {
        float offset = Time.time * scrollSpeed;
        lava.material.SetTextureOffset("_MainTex", new Vector2(0, offset));
    }
}
