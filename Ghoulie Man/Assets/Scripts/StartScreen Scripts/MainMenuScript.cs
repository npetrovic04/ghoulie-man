﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    //private CanvasGroup loadingScreen;

    //private Image progressBar;

    //private void Awake()
    //{
    //    loadingScreen = GameObject.Find("LoadingCanvas").GetComponent<CanvasGroup>();
    //    progressBar = GameObject.Find("LoadingCanvas/ProgressBarHolder/ProgressBar").GetComponent<Image>();
    //}

    //public void PlayGame()
    //{
    //    //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    //    StartCoroutine(AsyncSceneLoad());
    //}

    //public void QuitGame()
    //{
    //    print("Quit Game");
    //    Application.Quit();
    //}

    //IEnumerator AsyncSceneLoad()
    //{
    //    AsyncOperation operation = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);

    //    OpenCanvasGroup(loadingScreen);

    //    while (!operation.isDone)
    //    {
    //        float progress = Mathf.Clamp01(operation.progress / .9f);

    //        progressBar.fillAmount = operation.progress;
    //        yield return null;
    //    }
    //}


    //private void OpenCanvasGroup(CanvasGroup cg)
    //{
    //    cg.alpha = 1;
    //    cg.interactable = true;
    //    cg.blocksRaycasts = true;
    //}

    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronuisly(sceneIndex));
    }

    IEnumerator LoadAsynchronuisly(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            slider.value = progress;
            progressText.text = progress * 100f + "%";

            yield return null;
        }
    }
}
