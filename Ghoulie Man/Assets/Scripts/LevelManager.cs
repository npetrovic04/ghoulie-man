﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public float timer = 0;
    public float waitTime = 2f;

    public GameObject currentCheckpoint;
    private GameObject player;
    private PlayerHealth playerHealth;
    private CharacterMovement characterMovement;

    public Animator animator;
    private LifeManager lifeSystem;

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.instance.Player;
        playerHealth = player.GetComponent<PlayerHealth>();
        characterMovement = player.GetComponent<CharacterMovement>();
        animator = player.GetComponent<Animator>();
        lifeSystem = FindObjectOfType<LifeManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RespawnPlayer()
    {
        timer = timer + Time.deltaTime;
        if(timer >= waitTime)
        {
            lifeSystem.TakeLife();
            player.transform.position = currentCheckpoint.transform.position;
            playerHealth.CurrentHealth = 100;
            playerHealth.HealthSlider.value = playerHealth.CurrentHealth;
            timer = 0;
            characterMovement.enabled = true;
            animator.Play("Blend Tree");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex );
        }
    }
}
