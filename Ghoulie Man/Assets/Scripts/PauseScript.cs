﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    public static bool gameIsPaused = false;
    public GameObject audioManager;
    private AudioSource audioSource;

    private AudioSource pauseAudio;
    public AudioClip pause;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = audioManager.GetComponent<AudioSource>();
        pauseAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    if (gameIsPaused)
        //    {
        //        Resume();
        //    }
        //    else
        //    {
        //        Pause();
        //    }
        //}
    }

    public void Resume()
    {
        if (gameIsPaused)
        {
            Time.timeScale = 1;
            gameIsPaused = false;
            audioSource.Play();
            pauseAudio.PlayOneShot(pause);
        }
        else
        {
            Time.timeScale = 0;
            gameIsPaused = true;
            audioSource.Pause();
            pauseAudio.PlayOneShot(pause);
        }
    }

    //public void Pause()
    //{
    //    if (!gameIsPaused)
    //    {
    //        Time.timeScale = 0;
    //        gameIsPaused = true;
    //        audioSource.Pause();
    //        pauseAudio.PlayOneShot(pause);
    //    }
        
    //}
}
