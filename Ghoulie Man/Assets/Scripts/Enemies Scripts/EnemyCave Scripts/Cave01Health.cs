﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cave01Health : MonoBehaviour
{
    [SerializeField] private int startingHealth = 50;
    [SerializeField] private float timeSinceLastHit = 0.2f;
    [SerializeField] private float dissapearSpeed = 2f;
    [SerializeField] private int currentHealth;

    private float timer = 0;
    private Animator anim;
    private bool isAlive;
    private Rigidbody rigidBody;
    private BoxCollider boxCollider;
    private bool dissapearEnemy;
    new AudioSource audio;
    public AudioClip hurtAudio;
    public AudioClip killAudio;
    private DropItems dropItem;
    public GameObject explosionEffect;

    public bool IsAlive
    {
        get { return isAlive; }
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();
        anim = GetComponent<Animator>();
        isAlive = true;
        currentHealth = startingHealth;
        audio = GetComponent<AudioSource>();
        dropItem = GetComponent<DropItems>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(timer >= timeSinceLastHit && !GameManager.instance.GameOver)
        {
            if(other.tag == "PlayerWeapon")
            {
                TakeHit();
                timer = 0;
            }
        }
    }

    private void TakeHit()
    {
        if(currentHealth > 0)
        {
            anim.Play("Hurt");
            currentHealth -= 10;
            audio.PlayOneShot(hurtAudio);
        }

        if(currentHealth <= 0)
        {
            isAlive = false;
            KillEnemy();

            StartCoroutine(Explotion());
        }
    }

    private void KillEnemy()
    {
        boxCollider.enabled = false;
        anim.SetTrigger("EnemyDie");
        audio.Stop();
        audio.PlayOneShot(killAudio);

        StartCoroutine(RemoveEnemy());
       // dropItem.Drop();

    }

    IEnumerator RemoveEnemy()
    {
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
    }

    IEnumerator Explotion()
    {
        yield return new WaitForSeconds(3f);
        GameObject newExplosionEffect = Instantiate(explosionEffect, transform.position, transform.rotation);
        Destroy(newExplosionEffect, 1.5f);
        dropItem.Drop();
    }
}
