﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy01Attack : MonoBehaviour
{
    [SerializeField] private float range = 3f;
    [SerializeField] private float timeBetweenAttacks = 1f;

    private Animator anim;
    public GameObject player;
    private bool playerInRange;
    private BoxCollider weaponCollider;
    private Enemy01Health enemy01Health;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        weaponCollider = GetComponentInChildren<BoxCollider>();
        enemy01Health = GetComponent<Enemy01Health>();

        StartCoroutine(Attack());
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, player.transform.position) < range && enemy01Health.IsAlive)
        {
            playerInRange = true;
        }
        else
        {
            playerInRange = false;
        }
    }

    

    IEnumerator Attack()
    {
        if(playerInRange && !GameManager.instance.GameOver)
        {
            anim.Play("Attacking");
            yield return new WaitForSeconds(timeBetweenAttacks);
        }
        yield return null;
        StartCoroutine(Attack());
    }
}
