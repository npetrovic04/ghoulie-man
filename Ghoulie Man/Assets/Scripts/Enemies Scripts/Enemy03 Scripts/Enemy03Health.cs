﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy03Health : MonoBehaviour
{
    [SerializeField] private int startingHealth = 10;
    [SerializeField] private int currentHealth;
    
    private Rigidbody rigidBody;
    private SphereCollider sphereCollider;
    new AudioSource audio;
    public AudioClip killAudio;
    public GameObject explosionEffect;


    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        sphereCollider = GetComponent<SphereCollider>();
        currentHealth = startingHealth;
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!GameManager.instance.GameOver)
        {
            if (other.tag == "PlayerWeapon" || other.name == "Player")
            {
                TakeHit();
            }
        }
    }

    private void TakeHit()
    {
        if (currentHealth > 0)
        {
            GameObject newExplosionEffect = Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(newExplosionEffect, 1);
            
            currentHealth -= 10;
        }

        if (currentHealth <= 0)
        {
            KillEnemy();
        }
    }

    private void KillEnemy()
    {
        sphereCollider.enabled = false;
        audio.PlayOneShot(killAudio);
        Destroy(gameObject);
    }
}
