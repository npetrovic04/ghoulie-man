﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy03Movement : MonoBehaviour
{
    public float moveSpeed;
    private Rigidbody rigidBody;
    new Transform transform;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        transform = GetComponent<Transform>();
    }
    
    private void FixedUpdate()
    {
        Vector2 Vel = rigidBody.velocity;
        Vel.x = -moveSpeed;
        rigidBody.velocity = Vel;
    }
}
