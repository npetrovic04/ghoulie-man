﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy02Health : MonoBehaviour
{
    [SerializeField] int startingHelath = 20;
    [SerializeField] float timeSinceLastHit = 0.5f;
    [SerializeField] float dissapearSpeed = 2f;
    [SerializeField] int currentHealth;

    private float timer = 0f;
    private Animator anim;
    private bool isAlive;
    private Rigidbody rigidBody;
    private CapsuleCollider capsuleCollider;
    private bool dissapearEnemy = false;

    private AudioSource audioSource;
    public AudioClip hurtAudio;
    public AudioClip deadAudio;

    private DropItems dropItems;

    public bool IsAlive
    {
        get
        {
            return isAlive;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        anim = GetComponent<Animator>();
        isAlive = true;
        currentHealth = startingHelath;
        audioSource = GetComponent<AudioSource>();
        dropItems = GetComponent<DropItems>();
    }
    
    void Update()
    {
        timer += Time.deltaTime;

        if (dissapearEnemy)
        {
            transform.Translate(-Vector3.up * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (timer >= timeSinceLastHit && !GameManager.instance.GameOver)
        {
            if (other.tag == "PlayerWeapon")
            {
                TakeHit();
                timer = 0;
            }
        }
    }

    void TakeHit()
    {
        if (currentHealth > 0)
        {
            anim.Play("Hurt");
            currentHealth -= 10;
            audioSource.PlayOneShot(hurtAudio);
        }
        if (currentHealth <= 0)
        {
            isAlive = false;
            KillEnemy();
        }
    }

    void KillEnemy()
    {
        capsuleCollider.enabled = false;
        anim.SetTrigger("EnemyDie");
        rigidBody.isKinematic = true;
        audioSource.PlayOneShot(deadAudio);

        StartCoroutine(RemoveEnemy());
        dropItems.Drop();
    }

    IEnumerator RemoveEnemy()
    {
        yield return new WaitForSeconds(dissapearSpeed);
        dissapearEnemy = true;
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
