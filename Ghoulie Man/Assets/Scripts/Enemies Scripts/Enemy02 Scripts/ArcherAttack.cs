﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherAttack : MonoBehaviour
{
    [SerializeField] float range = 10f;
    [SerializeField] float timeBetweenAttacks = 1f;

    public GameObject player;
    public bool playerInRange;
    private Animator anim;
    private Rigidbody clone;

    public float arrowSpeed = 60f;
    public Transform arrowSpawn;
    public Rigidbody arrowPrefab;

    // Start is called before the first frame update
    void Start()
    {
        //arrowSpawn = GameObject.Find("ArrowSpawn").transform;
        anim = GetComponent<Animator>();
        player = GameManager.instance.Player;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, player.transform.position) <= range)
        {
            playerInRange = true;
            anim.SetTrigger("Attacking");
        }
        else
        {
            playerInRange = false;
        }
    }

    public void FireArcherProjectile()
    {
        clone = Instantiate(arrowPrefab, arrowSpawn.position, arrowSpawn.rotation) as Rigidbody;
        clone.AddForce(-arrowSpawn.transform.right * arrowSpeed);
    }
}
