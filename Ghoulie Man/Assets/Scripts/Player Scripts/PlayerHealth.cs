﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] int startingHealth = 100;
    [SerializeField] float timeSinceLastHit = 2f;
    [SerializeField] int currentHealth;
    [SerializeField] float timer = 0;
    [SerializeField] Slider healthSlider;
    private CharacterMovement characterMovement;
    
    private Animator anim;
    private AudioSource audioSource;
    public AudioClip hurtAudio;
    public AudioClip deadAudio;
    public AudioClip pickItem;

    private ParticleSystem particle_System;
    public LevelManager levelManager;
    public AudioManager audioManager;

    public bool isDead;


    public int CurrentHealth
    {
        get
        {
            return currentHealth;
        }
        set
        {
            if(value < 0)
            {
                currentHealth = 0;
            }
            else
            {
                currentHealth = value;
            }
        }
    }

    public Slider HealthSlider
    {
        get { return healthSlider; }
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        currentHealth = startingHealth;
        characterMovement = GetComponent<CharacterMovement>();
        audioSource = GetComponent<AudioSource>();
        levelManager = FindObjectOfType<LevelManager>();
        audioManager = FindObjectOfType<AudioManager>();

        particle_System = GetComponent<ParticleSystem>();
        var emmision = particle_System.emission;
        emmision.enabled = false;
        isDead = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        PlayerKill();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(timer >= timeSinceLastHit && !GameManager.instance.GameOver)
        {
            if(other.tag == "Weapon")
            {
                TakeHit();
                timer = 0;
            }
        }
    }

    public void TakeHit()
    {
        if(currentHealth > 0)
        {
            GameManager.instance.PlayerHit(currentHealth);
            anim.Play("Hurt");
            currentHealth -= 10;
            healthSlider.value = currentHealth;
            audioSource.PlayOneShot(hurtAudio);
        }
        if (currentHealth <= 0)
        {
            GameManager.instance.PlayerHit(currentHealth);
            anim.SetTrigger("IsDead");
            characterMovement.enabled = false;
            audioManager.backgroundMusic.Stop();
            audioSource.PlayOneShot(deadAudio);
        }
    }

    public void PowerUpHealth()
    {
        if(currentHealth <= 80)
        {
            currentHealth += 20;
        }
        else if(currentHealth < startingHealth)
        {
            currentHealth = startingHealth;
        }
        healthSlider.value = currentHealth;
        audioSource.PlayOneShot(pickItem);
    }

    public void KillBox()
    {
        CurrentHealth = 0;
        healthSlider.value = currentHealth;
        audioManager.backgroundMusic.Stop();
        audioSource.PlayOneShot(deadAudio);
    }

    public void PlayerKill()
    {
        if(currentHealth <= 0)
        {
            characterMovement.enabled = false;
            levelManager.RespawnPlayer();
            //audioManager.backgroundMusic.Play();
            StartCoroutine(PlayMusicDelay());
        }
    }

    IEnumerator PlayMusicDelay()
    {
        yield return new WaitForSeconds(1.5f);
        audioManager.backgroundMusic.Play();
    }
}
