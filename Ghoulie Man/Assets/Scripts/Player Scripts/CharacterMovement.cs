﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class CharacterMovement : MonoBehaviour
{
    public float maxSpeed = 6f;
    public float moveDirection;
    public bool facingRight = true;
    public float jumpSpeed = 600.0f;

    public bool grounded = false;
    public Transform groundCheck;
    public float groundRadius = 0.2f;
    public LayerMask whatIsGround;

    public float knifeSpeed = 600.0f;
    public Transform knifeSpawn;
    public Rigidbody knifePrefab;
    Rigidbody clone;

    Rigidbody rigidBody;
    Animator animator;

    private AudioSource audioSource;
    public AudioClip knifeThrow;
    public AudioClip jumpSound;

    public GameObject explosionEffect;

    public bool testMode;


    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        groundCheck = GameObject.Find("GroundCheck").transform;
        knifeSpawn = GameObject.Find("KnifeSpawn").transform;
        audioSource = GetComponent<AudioSource>();
        GameObject newExplosionEffect = Instantiate(explosionEffect, transform.position, transform.rotation);
        Destroy(newExplosionEffect, 4f);
    }

    // Update is called once per frame
    void Update()
    {
        if (testMode)
        {
            moveDirection = Input.GetAxis("Horizontal");

            if (grounded && Input.GetButtonDown("Jump"))
            {
                animator.SetTrigger("IsJumping");
                rigidBody.AddForce(new Vector2(0, jumpSpeed));
                audioSource.PlayOneShot(jumpSound);
            }
        }
        else
        {
            moveDirection = CrossPlatformInputManager.GetAxis("Horizontal");

            if (grounded && CrossPlatformInputManager.GetButtonDown("Jump"))
            {
                animator.SetTrigger("IsJumping");
                rigidBody.AddForce(new Vector2(0, jumpSpeed));
                audioSource.PlayOneShot(jumpSound);
            }
        }

    }

    void FixedUpdate()
    {
        rigidBody.velocity = new Vector2(moveDirection * maxSpeed, rigidBody.velocity.y);

        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);

        if (moveDirection > 0.0f && !facingRight)
        {
            Flip();
        }
        else if (moveDirection < 0.0f && facingRight)
        {
            Flip();
        }
        animator.SetFloat("Speed", Mathf.Abs(moveDirection));

        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            Attack();
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(Vector3.up, 180f, Space.World);
    }

    void Attack()
    {
        animator.SetTrigger("Attacking");
    }

    void CallFireProjectile()
    {
        clone = Instantiate(knifePrefab, knifeSpawn.position, knifeSpawn.rotation) as Rigidbody;
        clone.AddForce(knifeSpawn.transform.right * knifeSpeed);
        audioSource.PlayOneShot(knifeThrow);
    }
}
