﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerItemScripts : MonoBehaviour
{
    public GameObject player;
    private PlayerHealth playerHealth;
    private ParticleSystem particle_System;
    private AudioSource audioSource;

    private MeshRenderer meshRenderer;
    private ParticleSystem brainParticles;
    public GameObject pickUpEffect;

    private ItemExplode itemExplode;
    private SphereCollider sphereCollider;
    

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        playerHealth = player.GetComponent<PlayerHealth>();                  // posto je ova skripta zalepljena na PowerItem (mozak)
        playerHealth.enabled = true;                                         // ona uzima Particle Effects od playera i desableuje ga.
                                                                             // Kada ga nema ovaj Game object na sceni, onda nema ko da preuzme i 
        particle_System = player.GetComponent<ParticleSystem>();             // ugasi partikle na playeru i onda partikli rade na playeru non stop
        var emmision = particle_System.emission;                             // zato se i playeru u start stavlja da se ugasi particle
        emmision.enabled = false;

        meshRenderer = GetComponentInChildren<MeshRenderer>();
        brainParticles = GetComponent<ParticleSystem>();

        itemExplode = GetComponent<ItemExplode>();
        sphereCollider = GetComponent<SphereCollider>();
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            StartCoroutine(InvincibleRoutine());
            meshRenderer.enabled = false;
        }
    }

    public IEnumerator InvincibleRoutine()
    {
        itemExplode.PickUp();
        var emmision = particle_System.emission;
        emmision.enabled = true;
        playerHealth.enabled = false;
        var emmisionBrain = brainParticles.emission;
        emmisionBrain.enabled = false;
        sphereCollider.enabled = false;

        yield return new WaitForSeconds(10f);
        emmision.enabled = false;
        playerHealth.enabled = true;
        Destroy(gameObject);
    }

    //void PickUp()
    //{
    //    Instantiate(pickUpEffect, transform.position, transform.rotation);
    //}
}
