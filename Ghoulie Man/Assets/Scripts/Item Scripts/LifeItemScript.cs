﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeItemScript : MonoBehaviour
{
    private GameObject player;
    private LifeManager lifeManager;
    private BoxCollider box_Collider;
    private SpriteRenderer sprite_Renderer;


    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.instance.Player;
        lifeManager = FindObjectOfType<LifeManager>();
        box_Collider = GetComponent<BoxCollider>();
        sprite_Renderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            PickLife();
        }
    }

    public void PickLife()
    {
        lifeManager.GiveLife();
        sprite_Renderer.enabled = false;
        box_Collider.enabled = false;
        Destroy(gameObject);
    }
}
