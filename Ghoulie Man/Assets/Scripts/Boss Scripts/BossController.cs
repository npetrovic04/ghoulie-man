﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public bool bossAwake = false;
    private Animator anim;

    public bool inBattle = false;
    public bool attacking = false;
    public float idleTimer = 0f;
    public float idleWaitTime = 10f;

    private BossHealth bossHealth;
    public float attackTimer = 0;
    public float attackWaitTime = 4f;

    public BoxCollider swordTrigger, bossCheckPoint;
    public GameObject bossHelathBar;

    private SmoothFollow smoothFollow;
    private GameObject player;
    private PlayerHealth playerHealth;
    new ParticleSystem particleSystem;
    

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        bossHealth = GetComponentInChildren<BossHealth>();
        swordTrigger = GameObject.Find("Boss").GetComponentInChildren<BoxCollider>();
        bossHelathBar.SetActive(false);
        smoothFollow = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SmoothFollow>();
        player = GameManager.instance.Player;
        playerHealth = player.GetComponent<PlayerHealth>();
        bossCheckPoint = GameObject.Find("BossCheckPoint").GetComponent<BoxCollider>();
        particleSystem = GameObject.Find("RockParticleSysyem").GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (bossAwake)
        {
            anim.SetBool("bossAwake", true);
            bossHelathBar.SetActive(true);
        }

        if (inBattle)
        {
            if (!attacking)
            {
                idleTimer += Time.deltaTime;
            }
            else
            {
                idleTimer = 0;
                attackTimer += Time.deltaTime;
                if(attackTimer >= attackWaitTime)
                {
                    RandomAttacks();
                }
            }
            if (idleTimer > idleWaitTime)
            {
                attacking = true;
                idleTimer = 0f;
            }
        }
        else
        {
            idleTimer = 0f;
        }

        if(bossHealth.bossHealth > 0 && playerHealth.CurrentHealth > 0)
        {
            if(bossHealth.bossHealth > 15)
            {
                attackWaitTime = 4f;
            }
            if (bossHealth.bossHealth > 10 && bossHealth.bossHealth < 16)
            {
                attackWaitTime = 3f;
            }
            if (bossHealth.bossHealth > 5 && bossHealth.bossHealth < 11)
            {
                attackWaitTime = 2f;
            }
            if (bossHealth.bossHealth > 1 && bossHealth.bossHealth < 6)
            {
                attackWaitTime = 1f;
            }
        }

        BossReset();
    }

    void RandomAttacks()
    {
        switch (Random.Range(0,3))
        {
            case 0:
                BossAttack01();
                break;
            case 1:
                BossAttack02();
                break;
            case 2:
                BossAttack03();
                break;
            default:
                break;
        }
    }

    private void BossAttack01()
    {
        anim.SetTrigger("bossAttack");
        attacking = false;
        attackTimer = 0;
        idleWaitTime = 3;
        swordTrigger.enabled = true;
    }

    private void BossAttack02()
    {
        anim.SetTrigger("bossAttack02");
        attacking = false;
        attackTimer = 0;
        idleWaitTime = 3;
        swordTrigger.enabled = true;
    }

    private void BossAttack03()
    {
        anim.SetTrigger("bossAttack03");
        attacking = false;
        attackTimer = 0;
        swordTrigger.enabled = true;
        StartCoroutine(FallingRocks());
    }

    IEnumerator FallingRocks()
    {
        yield return new WaitForSeconds(2);
        var emmision = particleSystem.emission;
        emmision.enabled = true;
        particleSystem.Play();
        yield return new WaitForSeconds(3);
        emmision.enabled = false;
    }

    void BossReset()
    {
        if(playerHealth.CurrentHealth == 0)
        {
            bossAwake = false;
            bossCheckPoint.isTrigger = true;
            smoothFollow.cameraBossActive = false;
            anim.Play("Idle");
            anim.SetBool("bossAwake", false);
            bossHealth.bossHealth = 20;
        }
    }
}
