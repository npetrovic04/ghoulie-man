﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class BossHealth : MonoBehaviour
{
    public int bossHealth = 20;
    private Animator anim;
    public bool bossDead = false;
    public BossController bossController;

    private CapsuleCollider capsuleCollider;
    private BoxCollider weaponCollider;
    private SphereCollider triggerCollider;
    new ParticleSystem particleSystem;

    public Material hurtBossMaterial;
    private GameObject bossModel;

    public GameObject videoPlayer;
    public AudioClip newTrack02;
    private AudioManager audioManager;

    // Start is called before the first frame update
    void Start()
    {
        anim = GameObject.Find("Boss").GetComponent<Animator>();
        bossController = GameObject.Find("Boss").GetComponent<BossController>();
        capsuleCollider = GameObject.Find("Boss").GetComponent<CapsuleCollider>();
        weaponCollider = GameObject.Find("Boss").GetComponentInChildren<BoxCollider>();
        triggerCollider = GameObject.Find("Boss").GetComponentInChildren<SphereCollider>();
        particleSystem = GameObject.Find("RockParticleSysyem").GetComponent<ParticleSystem>();
        bossModel = GameObject.FindGameObjectWithTag("BossModel");
        videoPlayer.SetActive(false);
        audioManager = FindObjectOfType<AudioManager>();

        Advertisement.Initialize("3167723", true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "PlayerWeapon" && bossHealth > 0)
        {
            anim.SetTrigger("isHit");
            bossHealth--;
            if(bossHealth < 6)
            {
                bossModel.GetComponent<SkinnedMeshRenderer>().material = hurtBossMaterial;
            }
        }
        else
        {
            BossDead();
        }
    }

    void BossDead()
    {
        bossDead = true;
        anim.SetTrigger("isDead");
        bossController.bossAwake = false;
        bossController.inBattle = false;
        //bossController.swordTrigger.enabled = false;
        capsuleCollider.enabled = false;
        weaponCollider.enabled = false;
        triggerCollider.enabled = false;
        var emmision = particleSystem.emission;
        emmision.enabled = false;
        StartCoroutine(PlayVideo());

        if (newTrack02 != null)
        {
            audioManager.ChangeMusic02(newTrack02);
        }
    }

    IEnumerator PlayVideo()
    {
        yield return new WaitForSeconds(5);
        videoPlayer.SetActive(true);
        yield return new WaitForSeconds(7);
        ShowAdWhenReady();
        SceneManager.LoadScene(0);
    }

    void ShowAdWhenReady()
    {
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
    }
}
